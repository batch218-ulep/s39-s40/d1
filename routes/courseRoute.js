const express = require("express");
const router = express.Router();
const Course = require("../models/course.js");
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js")

// Create single course
router.post("/create", auth.verify, (req, res) => {
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  courseController.addCourse(data).then(result => {
    res.send(result)
  })
})

// Get all courses
router.get("/all", (req, res) => {
  courseController.getAllCourses().then(result => {
    res.send(result)
  })
})

// Get all ACTIVE courses
router.get("/active", (req, res) => {
  courseController.getActiveCourses().then(result => {
    res.send(result)
  })
})

// Get single course
router.get("/:courseId", (req, res) => {
  courseController.getCourse(req.params.courseId).then(result => {
    res.send(result)
  })
})

// Updating a single course
router.patch("/:courseId/update", auth.verify, (req, res) => {
  courseController.updateCourse(req.params.courseId, req.body).then(result => {
    res.send(result)
  })
})

// Archiving a single course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
  courseController.archiveCourse(req.params.courseId).then(result => {
    res.send(result)
  })
})

module.exports = router

